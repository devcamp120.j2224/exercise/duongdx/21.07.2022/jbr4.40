package com.devcamp.s50.jbr4_40;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class invoicesController {
    @CrossOrigin
    @GetMapping("/invoices")
    public ArrayList<Invoice> getInvoicesApi(){
        Customer Khach1 = new Customer(1, "linh", 10);
        Customer Khach2 = new Customer(2, "thuy", 15);
        Customer Khach3 = new Customer(3, "le", 20);

        System.out.println(Khach1);
        System.out.println(Khach2);
        System.out.println(Khach3);

        Invoice HoaDon1 = new Invoice(111, Khach1, 120000);
        Invoice HoaDon2 = new Invoice(112, Khach1, 160000);
        Invoice HoaDon3 = new Invoice(113, Khach1, 420000);

        HoaDon1.getAmountAfterDiscount();
        HoaDon2.getAmountAfterDiscount();
        HoaDon3.getAmountAfterDiscount();

        ArrayList<Invoice> DanhSachHoaDon = new ArrayList<>();
        DanhSachHoaDon.add(HoaDon1);
        DanhSachHoaDon.add(HoaDon2);
        DanhSachHoaDon.add(HoaDon3);

        return DanhSachHoaDon ;
    }
}
